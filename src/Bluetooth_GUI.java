import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

public class Bluetooth_GUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField txtCommand;
	private String line = "";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Bluetooth_GUI frame = new Bluetooth_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Bluetooth_GUI() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout(0, 0));

		final JTextPane textPane = new JTextPane();
		textPane.setSize(new Dimension(20, 20));
		textPane.setMinimumSize(new Dimension(300, 300));
		textPane.setEditable(false);
		getContentPane().add(textPane, BorderLayout.NORTH);

		txtCommand = new JTextField();
		txtCommand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				line=txtCommand.getText();
				textPane.setText(textPane.getText() + "\n" + line);
			}
		});
		getContentPane().add(txtCommand, BorderLayout.SOUTH);
		txtCommand.setColumns(10);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		textPane.setText("Iniciando aplicacion Bluetooth. \n");

		JButton btnBuscarDispositivos = new JButton("Buscar dispositivos");
		btnBuscarDispositivos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					List<String> res = getDevices();
					for (String str : res) {
						textPane.setText(textPane.getText() + str);
					}
				} catch (IOException | InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					textPane.setText(textPane.getText() + "\n"
							+ "Fallo en la aplicacion");
					
				}
			}
		});
		panel.add(btnBuscarDispositivos);

		JButton btnBuscarServicios = new JButton("Buscar servicios");
		panel.add(btnBuscarServicios);
		btnBuscarServicios.setHorizontalAlignment(SwingConstants.RIGHT);
		btnBuscarServicios.setAlignmentY(Component.TOP_ALIGNMENT);

		JButton btnBuscarUnDispositivo = new JButton("Buscar un dispositivo");
		btnBuscarUnDispositivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textPane.setText(textPane.getText() + "\n" + "Introduzca el friendly-name o la direcci�n del dispositivo. ");
				String ac = e.getActionCommand();
				System.out.println(ac);
			}
		});
		panel.add(btnBuscarUnDispositivo);

		JButton btnBuscarUnServicio = new JButton("Buscar un servicio");
		panel.add(btnBuscarUnServicio);

	}

	public List<String> getDevices() throws IOException, InterruptedException {
		final Vector<RemoteDevice> devicesDiscovered = new Vector<RemoteDevice>();
		final List<String> result = new LinkedList<>();
		final Object inquiryCompletedEvent = new Object();

		devicesDiscovered.clear();

		DiscoveryListener listener = new DiscoveryListener() {

			public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
				// System.out.println("Dispositivo " +
				// btDevice.getBluetoothAddress() + " encontrado.");
				result.add("Dispositivo " + btDevice.getBluetoothAddress()
						+ " encontrado. \n");
				devicesDiscovered.addElement(btDevice);
				try {
					// System.out.println("Nombre " +
					// btDevice.getFriendlyName(false));
					result.add("Nombre " + btDevice.getFriendlyName(false)+ " \n");
				} catch (IOException cantGetDeviceName) {
				}
			}

			public void inquiryCompleted(int discType) {
				// System.out.println("Busqueda de dispositivos completada satisfactoriamente.\n");
				result.add("Busqueda de dispositivos completada satisfactoriamente.\n");
				synchronized (inquiryCompletedEvent) {
					inquiryCompletedEvent.notifyAll();
				}
			}

			public void serviceSearchCompleted(int transID, int respCode) {
			}

			public void servicesDiscovered(int transID,
					ServiceRecord[] servRecord) {
			}
		};

		synchronized (inquiryCompletedEvent) {
			boolean started = LocalDevice.getLocalDevice().getDiscoveryAgent()
					.startInquiry(DiscoveryAgent.GIAC, listener);
			if (started) {
				// System.out.println("Realizandose busqueda de dispositivos. \n");
				result.add("Realizandose busqueda de dispositivos. \n");
				inquiryCompletedEvent.wait();
				// System.out.println(devicesDiscovered.size() +
				// " dispositivo(s) disponible(s)");
				result.add(devicesDiscovered.size()
						+ " dispositivo(s) disponible(s)");
			}
		}
		return result;
	}

	public List<String> getADevice(String nameOfDevice) throws InterruptedException, IOException {
		final Vector<RemoteDevice> devicesDiscovered = new Vector<RemoteDevice>();
		System.out
				.println("Introduzca la direccion a la que desea conectarse: ");
		List<String> result = new LinkedList<>();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final String Direccion = br.readLine();

		final Object inquiryCompletedEvent = new Object();

		devicesDiscovered.clear();

		System.out.println("Iniciando busqueda");

		LocalDevice ld = LocalDevice.getLocalDevice();

		DiscoveryAgent da = ld.getDiscoveryAgent();

		DiscoveryListener listener = new DiscoveryListener() {

			public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {

				if (btDevice.getBluetoothAddress().equals(Direccion)) {
					System.out.println("Dispositivo "
							+ btDevice.getBluetoothAddress() + " encontrado.");

					devicesDiscovered.addElement(btDevice);

					try {

						System.out.println("Nombre "
								+ btDevice.getFriendlyName(false));

					} catch (IOException cantGetDeviceName) {

					}

				}

			}

			public void inquiryCompleted(int discType) {
				System.out.println("Busqueda de dispositivo completada.");

				synchronized (inquiryCompletedEvent) {

					inquiryCompletedEvent.notifyAll();

				}

			}

			public void serviceSearchCompleted(int transID, int respCode) {

			}

			public void servicesDiscovered(int transID,
					ServiceRecord[] servRecord) {

			}

		};

		synchronized (inquiryCompletedEvent) {

			/*
			 * 
			 * LocalDevice ld= LocalDevice.getLocalDevice();
			 * 
			 * DiscoveryAgent da= ld.getDiscoveryAgent();
			 */

			boolean started = da.startInquiry(DiscoveryAgent.GIAC, listener);

			if (started) {
				
				System.out.println("Espere que la busqueda termine.");

				inquiryCompletedEvent.wait();

				System.out.println(devicesDiscovered.size()
						+ " dispositivo encontrado.");

			}

		}
		return result;

	}

}
