import java.io.IOException;
import java.util.Enumeration;
import java.util.Random;
import java.util.Vector;

import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;

public class SerialPortDiscover {

	static final UUID OBEX_FILE_TRANSFER = new UUID(0x1106);
	static final UUID PUBLIC_BROWSE_GROUP = new UUID(0x1002);
	static final int SERVICE_NAME_ATTRID = (0x0100);
	static final int SERVICE_DESCRIPTION_ATTRID = (0x0101);
	static final int PROVIDER_NAME_ATTRID = (0x0102);
	static final int SERIAL_PORT = (0x1101);

	public static final Vector<String> serviceFound = new Vector<String>();

	public static void main(String[] args) throws IOException,
			InterruptedException {

		// First run RemoteDeviceDiscovery and use discoved device

		RemoteDeviceDiscovery.main(null);
		System.out
				.println("Programa para descubrir un servicio p�blico y un servicio de clase serial port");
		serviceFound.clear();

		UUID serviceUUID = OBEX_FILE_TRANSFER;

		if ((args != null) && (args.length > 0)) {

			serviceUUID = new UUID(args[0], false);

		}

		final Object serviceSearchCompletedEvent = new Object();

		DiscoveryListener listener = new DiscoveryListener() {

			public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
			}

			public void inquiryCompleted(int discType) {
			}

			public void servicesDiscovered(int transID,
					ServiceRecord[] servRecord) {

				Random generator = new Random();
				int service_public = generator.nextInt(servRecord.length);

				System.out.println("Servicio :"
						+ "info: "
						+ servRecord[service_public]
								.getAttributeValue(servRecord[service_public]
										.getAttributeIDs()[1]));
				for(int j = 0; j<servRecord.length; j++) {
					if(servRecord[j].getAttributeValue(SERIAL_PORT) != null) {
						System.out.println("Servicio de tipo puerto serie encontrado");
						System.out.println("Connection url : "+ servRecord[j].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false));
					}
				}
			}

			public void serviceSearchCompleted(int transID, int respCode) {
				System.out.println("Busqueda completa");
				synchronized (serviceSearchCompletedEvent) {
					serviceSearchCompletedEvent.notifyAll();
				}
			}
		};

		// UUID[] searchUuidSet = new UUID[] { serviceUUID };
		UUID searchUuidSet[] = new UUID[1];
		searchUuidSet[0] = PUBLIC_BROWSE_GROUP;
		int attrIDs[] = new int[3];
		attrIDs[0] = SERVICE_NAME_ATTRID;
		attrIDs[1] = SERVICE_DESCRIPTION_ATTRID;
		attrIDs[2] = PROVIDER_NAME_ATTRID;

		// int[] attrIDs = new int[] { 0x0100}; // Service name

		for (Enumeration en = RemoteDeviceDiscovery.devicesDiscovered
				.elements(); en.hasMoreElements();) {

			RemoteDevice btDevice = (RemoteDevice) en.nextElement();
			synchronized (serviceSearchCompletedEvent) {
				System.out.println("Buscar servicios de "
						+ btDevice.getBluetoothAddress() + " "
						+ btDevice.getFriendlyName(false));
				LocalDevice
						.getLocalDevice()
						.getDiscoveryAgent()
						.searchServices(attrIDs, searchUuidSet, btDevice,
								listener);
				serviceSearchCompletedEvent.wait();
			}
		}
	}

}
