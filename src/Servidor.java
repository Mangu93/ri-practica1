import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

public class Servidor {
	private static final String SERVICE_NAME = "Echo Server";
	private static final String SERVICE_UUID_STRING = "5F6C6A6E1CFA49B49C831E0D1C9B9DC9";
	private static final UUID SERVICE_UUID = new UUID(SERVICE_UUID_STRING,
			false);
	//Y si lo pongo a true?
	private LocalDevice localDevice;

	public void init() throws BluetoothStateException {
		localDevice = LocalDevice.getLocalDevice();
	}

	public void start() throws BluetoothStateException, IOException {

		/*
		 * String connUrl = "btspp://localhost:" + SERVICE_UUID_STRING + ";" +
		 * "name=" + SERVICE_NAME;
		 */
		String connUrl = "btspp://localhost:"
				+ localDevice.getBluetoothAddress() + ";" + "name="
				+ SERVICE_NAME+";authenticate=false;encrypt=false;";
		StreamConnectionNotifier scn = (StreamConnectionNotifier) Connector
				.open(connUrl);
		System.out.println("Listo para aceptar conexiones \n");
		System.out.println("Direcci�n bluetooth :" + localDevice.getBluetoothAddress());
		System.out.println("Friendly name : "+ localDevice.getFriendlyName());

		while (true) {
			StreamConnection sc = (StreamConnection) scn.acceptAndOpen();
			RemoteDevice remoteDevice = RemoteDevice.getRemoteDevice(sc);
			String remoteAddress = remoteDevice.getBluetoothAddress();

			System.out.println("Conexion recibida de " + remoteAddress);

			String remoteName = "";
			try {
				remoteName = remoteDevice.getFriendlyName(false);
			} catch (IOException e) {
				System.err.println("Incapaz de realizar la conexi�n.");
			}

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					sc.openDataInputStream()));
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(
					sc.openDataOutputStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(remoteName + " (" + remoteAddress + "): "
						+ line);
				writer.println(line);
				writer.flush();
			}

			sc.close();

			System.out.println("Conexion con " + remoteAddress + " cerrada");
		}
	}

	public static void main(String[] args) {
		try {
			Servidor server = new Servidor();
			server.init();
			server.start();
		} catch (BluetoothStateException e) {

		} catch (IOException ex) {
		}
	}

}
