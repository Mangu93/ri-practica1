import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Vector;

import javax.bluetooth.DataElement;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;

public class ConcreteServiceSearch {

	static final UUID OBEX_FILE_TRANSFER = new UUID(0x0100);
	static final int SERVICE_ID = 0x0100;

	public static final Vector<String> serviceFound = new Vector<String>();

	public static void main(String[] args) throws IOException,
			InterruptedException {

		RemoteDeviceDiscovery.main(null);
		System.out
				.println("Busqueda de un servicio con un nombre concreto en un dispositivo concreto.");
		System.out
				.println("Introduzca la direccion del dispositivo :");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final String Direccion = br.readLine();
		System.out
				.println("Introduzca el nombre del servicio que quiera descubrir.");
		final String servicio_a_descubrir = br.readLine();

		serviceFound.clear();

		UUID serviceUUID = OBEX_FILE_TRANSFER;

		if ((args != null) && (args.length > 0)) {

			serviceUUID = new UUID(args[0], false);

		}

		final Object serviceSearchCompletedEvent = new Object();

		DiscoveryListener listener = new DiscoveryListener() {

			public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {

			}

			public void inquiryCompleted(int discType) {

			}

			public void servicesDiscovered(int transID,
					ServiceRecord[] servRecord) {
				int i = 0;
				while (i < servRecord.length) {

					@SuppressWarnings("deprecation")
					String sv = servRecord[i]
							.getAttributeValue(
									servRecord[i].getAttributeIDs()[1])
							.toString().toLowerCase();

					String svv = servicio_a_descubrir.toLowerCase();
					if (sv.contains(svv)) {

						System.out.println("Se encontr� el servicio "
								+ servicio_a_descubrir);
						System.out.println("Servicio: "
								+ i
								+ " info: "
								+ servRecord[i].getAttributeValue(servRecord[i]
										.getAttributeIDs()[1]));
						i = servRecord.length;

					}
					i++;
				}

			}

			public void serviceSearchCompleted(int transID, int respCode) {

				System.out.println("Busqueda completada");

				synchronized (serviceSearchCompletedEvent) {

					serviceSearchCompletedEvent.notifyAll();

				}

			}

		};

		UUID[] searchUuidSet = new UUID[] { serviceUUID };

		int[] attrIDs = new int[] { 0x0100 // Service name

		};

		for (Enumeration en = RemoteDeviceDiscovery.devicesDiscovered
				.elements(); en.hasMoreElements();) {

			RemoteDevice btDevice = (RemoteDevice) en.nextElement();
			synchronized (serviceSearchCompletedEvent) {
				if (btDevice.getBluetoothAddress().equals(Direccion)) {
					System.out.println("Buscar el servicio"+servicio_a_descubrir+"  en "
							+ btDevice.getBluetoothAddress() + " "
							+ btDevice.getFriendlyName(false));

					LocalDevice
							.getLocalDevice()
							.getDiscoveryAgent()
							.searchServices(attrIDs, searchUuidSet, btDevice,
									listener);

					serviceSearchCompletedEvent.wait();
				}
			}

		}

	}

}
