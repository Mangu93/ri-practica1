import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;

public class Bluetooth_Main {
	public static final Vector<RemoteDevice> devicesDiscovered = new Vector();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Testing bluetooth app. \n");
		try {
			LocalDevice ld = LocalDevice.getLocalDevice();
			System.out
					.println("Bluetooth adress : " + ld.getBluetoothAddress());
			System.out.println("Friendly name : " + ld.getFriendlyName());
			if (ld.getDiscoverable() == DiscoveryAgent.NOT_DISCOVERABLE) {
				System.out.println("The bluetooth device is not discoverable");
			} else {
				System.out.println("The bluetooth device is discoverable");
			}
			System.out.println("Quieres ver los demas dispositivos?");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    	String decision = br.readLine();
	    	if(decision.equalsIgnoreCase("Si")) {
	    		discoverAllDevices();
	    	}
	    	System.out.println("Quieres buscar un dispositivo en concreto?"); 
	    	decision = br.readLine();
	    	if(decision.equalsIgnoreCase("Si")) {
	    		
	    	}
		} catch (BluetoothStateException e) {
			System.err.println(e.getMessage());
		}catch (IOException ex) {
			System.err.println(ex.getMessage());
		}catch(InterruptedException exx) {
			System.err.println(exx.getMessage());
		}

	}

	public static void discoverAllDevices() throws BluetoothStateException, InterruptedException {
		final Object inquiryCompletedEvent = new Object();

		devicesDiscovered.clear();

		DiscoveryListener listener = new DiscoveryListener() {

			public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
				System.out.println("Dispositivo "
						+ btDevice.getBluetoothAddress() + " encontrado.");
				devicesDiscovered.addElement(btDevice);
				try {
					System.out.println("Nombre "
							+ btDevice.getFriendlyName(false));
				} catch (IOException cantGetDeviceName) {
				}
			}

			public void inquiryCompleted(int discType) {
				System.out
						.println("Busqueda de dispositivos completada satisfactoriamente.\n");
				synchronized (inquiryCompletedEvent) {
					inquiryCompletedEvent.notifyAll();
				}
			}

			public void serviceSearchCompleted(int transID, int respCode) {
			}

			public void servicesDiscovered(int transID,
					ServiceRecord[] servRecord) {
			}
		};

		synchronized (inquiryCompletedEvent) {
			boolean started = LocalDevice.getLocalDevice().getDiscoveryAgent()
					.startInquiry(DiscoveryAgent.GIAC, listener);
			if (started) {
				System.out.println("Realizandose busqueda de dispositivos. \n");
				inquiryCompletedEvent.wait();
				System.out.println(devicesDiscovered.size()
						+ " dispositivo(s) disponible(s)");
			}
		}
	}

}
