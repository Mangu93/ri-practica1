import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

import javax.bluetooth.*;

/**
 * 
 * Minimal Device Discovery example.
 */

public class ConcreteDeviceDiscovery {

	public static final Vector<RemoteDevice> devicesDiscovered = new Vector();

	public static void main(String[] args) throws IOException,
			InterruptedException {
		System.out.println("Programa para descubrir un dispositivo en concreto");
		System.out
				.println("Introduzca la direccion que desea buscar: ");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final String Direccion = br.readLine();

		final Object inquiryCompletedEvent = new Object();

		devicesDiscovered.clear();

		System.out.println("Iniciando busqueda");

		LocalDevice ld = LocalDevice.getLocalDevice();

		DiscoveryAgent da = ld.getDiscoveryAgent();

		DiscoveryListener listener = new DiscoveryListener() {

			public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {

				if (btDevice.getBluetoothAddress().equals(Direccion)) {
					System.out.println("Dispositivo "
							+ btDevice.getBluetoothAddress() + " encontrado.");

					devicesDiscovered.addElement(btDevice);

					try {

						System.out.println("Nombre "
								+ btDevice.getFriendlyName(false));

					} catch (IOException cantGetDeviceName) {

					}

				}

			}

			public void inquiryCompleted(int discType) {
				System.out.println("Busqueda de dispositivo completada.");

				synchronized (inquiryCompletedEvent) {

					inquiryCompletedEvent.notifyAll();

				}

			}

			public void serviceSearchCompleted(int transID, int respCode) {

			}

			public void servicesDiscovered(int transID,
					ServiceRecord[] servRecord) {

			}

		};

		synchronized (inquiryCompletedEvent) {

			/*
			 * 
			 * LocalDevice ld= LocalDevice.getLocalDevice();
			 * 
			 * DiscoveryAgent da= ld.getDiscoveryAgent();
			 */

			boolean started = da.startInquiry(DiscoveryAgent.GIAC, listener);

			if (started) {

				System.out.println("Espere que la busqueda termine.");

				inquiryCompletedEvent.wait();

				System.out.println(devicesDiscovered.size()
						+ " dispositivo encontrado.");

			}

		}

	}

}