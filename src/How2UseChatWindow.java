/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

import chatUI.*;

/**
 *
 * @author pinilla
 */
public class How2UseChatWindow {
	private static final String SERVICE_NAME = "Echo Server";

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				final chatUI.ChatWindow _window;
				_window = new ChatWindow();
				// Iniciando bluetooth, si supera todo esto funciona
				try {
					BufferedReader br = new BufferedReader(
							new InputStreamReader(System.in));
					System.out
							.println("�Va a funcionar como servidor? Introduzca si o no");
					final String decision = br.readLine();
					if (decision.equalsIgnoreCase("si")) {
						LocalDevice localDevice = LocalDevice.getLocalDevice();
						String connUrl = "btspp://localhost:"
								+ localDevice.getBluetoothAddress() + ";"
								+ "name=" + SERVICE_NAME
								+ ";authenticate=false;encrypt=false;";
						StreamConnectionNotifier scn = (StreamConnectionNotifier) Connector
								.open(connUrl);
						_window.setVisible(true);
						_window.setOut("Listo para aceptar conexiones");
						_window.setOut("Direcci�n bluetooth :" + localDevice.getBluetoothAddress());
						_window.setOut("Friendly name : "+ localDevice.getFriendlyName());
						/*while(_window.isVisible()){
							StreamConnection sc = (StreamConnection) scn.acceptAndOpen();
							RemoteDevice remoteDevice = RemoteDevice.getRemoteDevice(sc);
							String remoteAddress = remoteDevice.getBluetoothAddress();
							_window.setOut("Conexion recibida de " + remoteAddress);
							String remoteName = remoteDevice.getFriendlyName(false);	
						}*/
					} else {
						RemoteDeviceDiscovery.main(null);
						System.out
								.println("Introduzca la direccion a la que desea conectarse.");

						final String Direccion = br.readLine();
					}
				} catch (IOException | InterruptedException e1) {

					e1.printStackTrace();
				}
				_window.setVisible(true);
				_window.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {

						String s = _window.getIn(); // metodo que lee de la
													// entrada
						_window.setOut(s); // Método que escribe en la salida
											// de la ventana

					}
				});
				/**
				 *
				 * Hay que registrar un listener para los eventos de ventana y
				 * sobre el de window closing, realizar el cierre de conexiones.
				 *
				 *
				 */

				_window.addWindowListener(new java.awt.event.WindowListener() {
					public void windowClosing(WindowEvent e) {
						System.out
								.println("Cerrando ventana, cerrando conexiones.");
					}

					public void windowClosed(WindowEvent e) {
						System.out.println("Ventana cerrada. ");
					}

					public void windowDeactivated(WindowEvent e) {
						// System.out.println("Ventana minimizada ");
					}

					public void windowOpened(WindowEvent e) {
						// System.out.println("Iniciando chat ");
					}

					public void windowIconified(WindowEvent e) {
						// System.out.println("Window event ");
					}

					public void windowDeiconified(WindowEvent e) {
						// System.out.println("Window event ");
					}

					public void windowActivated(WindowEvent e) {
						// System.out.println("Ventana maximizada ");
					}

				});

			}
		});
	}
}
