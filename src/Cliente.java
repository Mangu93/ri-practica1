
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
/**
 * 
 * @author Adrian Marin
 *
 */
public class Cliente {
	private static final String SERVICE_UUID_STRING = "5F6C6A6E1CFA49B49C831E0D1C9B9DC9";
	private static final UUID SERVICE_UUID = new UUID(SERVICE_UUID_STRING,
			false);
	private static final String SERVICE_NAME = "Echo";
	private LocalDevice localDevice;
	private DiscoveryAgent discoveryAgent;

	public void init() throws BluetoothStateException {
		localDevice = LocalDevice.getLocalDevice();
		discoveryAgent = localDevice.getDiscoveryAgent();
	}

	public void connect() throws IOException, InterruptedException {
		
		
		RemoteDeviceDiscovery.main(null);
		String connectionUrl = discoveryAgent.selectService(SERVICE_UUID,
				ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
		System.out.println("Introduzca direcci�n a conectarse");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		final String Direccion = br.readLine();
		/*String connUrl = "btspp://localhost:"
				+ Direccion + ";" + "name="
				+ SERVICE_NAME+";authenticate=false;encrypt=false;";*/
		String connUrl = "btspp://"+ Direccion + ";" + "name="
				+ SERVICE_NAME+ ";authenticate=false;encrypt=false;";
		//System.out.println("Conectando con " + connectionUrl);
		System.out.println("Conectando con " + connUrl);
		//StreamConnection sc = (StreamConnection) Connector.open(connectionUrl);
		StreamConnection sc = (StreamConnection) Connector.open(connUrl);
		RemoteDevice remoteDevice = RemoteDevice.getRemoteDevice(sc);
		String remoteAddress = remoteDevice.getBluetoothAddress();

		String remoteName = "";
		try {
			remoteName = remoteDevice.getFriendlyName(false);
		} catch (IOException e) {
			System.err
					.println("Incapaz de conseguir el nombre del dispositivo remoto");
		}

		System.out.println("Conexion abierta con " + remoteAddress);

		BufferedReader consoleReader = new BufferedReader(
				new InputStreamReader(System.in));
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				sc.openDataInputStream()));
		PrintWriter writer = new PrintWriter(new OutputStreamWriter(
				sc.openDataOutputStream()));

		String line;
		while ((line = consoleReader.readLine()) != null) {
			writer.println(line);
			writer.flush();
			line = reader.readLine();
			System.out
					.println(remoteName + " (" + remoteAddress + "): " + line);
		}

		sc.close();
	}

	public static void main(String[] args) throws BluetoothStateException,
			IOException {
		try {
			Cliente client = new Cliente();
			client.init();
			client.connect();
		} catch (BluetoothStateException e) {

		} catch (IOException ex) {

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
